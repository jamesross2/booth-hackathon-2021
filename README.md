# Chicago Booth School of Business: 2021 Hackathon
#### April 10th, 2021

This is your primary resource for the Booth 2021 Hackathon. Use this document as the starting point for all of your work. The repository also contains guides, links, code stubs, and other material to help you get the most out of your hackathon experience.


<br>

# Goals & Expectations

We have designed a "mini-project" to help you glimpse life as a Research Professional (RP). Your mini-project will center around a covid-19 dataset that we will provide you, sourced from the Illinois Department of Public Health (IDPH). Your goal today is to present some research that improves our understanding of covid-19 in the United States, or a tool that improves our ability to do so. This will require you to move through some typical stages of a research project, and to present your results at the end of the day. These stages should help you experience some of the broad responsibilities of our RPs, and include:

* exploring an actual covid-19 data from the Illinois Department of Public Health (IDPH),
* reviewing knowledge from experts and existing literature,
* producing some research of your own, and
* presenting your results at the conclusion of the day.

Before starting, take the time to read this document fully, and then discuss with your team. We always recommend coming up with a good plan before diving right in! You should have a mentor who will provide guidance throughout the day, and for any other questions you should have access to a Slack workspace. Good luck! :pray:


<br>

# Getting Started

These instructions are written for a general audience. Head to the Slack workspace if you'd like any help with any of the following.

1. Find this repository online. (We should have a link available.)
1. Download the repository directly, using [these instructions](https://docs.gitlab.com/ee/user/project/repository/#download-source-code).
1. Unzip the downloaded repository, and place all of the files into a new folder on your computer. Our folder is called `Booth-Hackathon-2021`.
1. Download the provided data. (We should have a link available for this, too.)
1. Put the data into a subfolder called `data/clean/[unzipped data here]`.
1. Start working from inside this repository!


If you're confident enough, you can also use some of the below tools. Keep in mind that none of these are necessary for your work today, but are often a part of best practice when it comes to good research practices. If you're not sure how to use any of these, we recommend spending your time working on the actual research tasks today&mdash;you can learn about these later.

* We like to use git for version control. If you're not familiar with this, don't worry about it right now, but you may want to [learn about it in the future](https://www.atlassian.com/git/tutorials/what-is-git).
* We also like to use virtual environments for consistency and reproducible research. Again, if this is unfamiliar, now probably isn't the right time to learn. But [this Stackoverflow post](https://stackoverflow.com/questions/23948317/why-is-virtualenv-necessary) has some ideas on why it is a good idea, and you can see [how to create one with Python](https://realpython.com/python-virtual-environments-a-primer/) or [how to use one with R](https://rstudio.github.io/renv/index.html). [Anaconda](https://docs.anaconda.com/anaconda/user-guide/getting-started/) can also handle both R and Python.
* Makefiles are invaluable for designing reproducible workflows, although they can be challenging to learn. If you're up for the challenge, read the [documentation here](http://web.mit.edu/gnu/doc/html/make_2.html).
* Using [`direnv`](https://direnv.net/) can make sure that your local environment is set up and any virtual environments are activated.


<br>

# COVID-19 data

Over the last 12 months or so, you may have heard of a pandemic that has been occuring throughout the world. We have collected some small amount of data from the [Illinois Department of Public Health](https://www.dph.illinois.gov/) and the [American Community Survey](https://www.census.gov/programs-surveys/acs/). We have broken these into three datasets for you to use as a starting point for your analysis. If it helps, we encourage you to find additional data to augment the datasets present. This data was collected on December 1st, 2020.

* __Cases__: this dataset summarises the number of covid tests and covid cases at a zipcode level. Some particularly interesting variables are:
    + `count_overall`: the number of cases in the zipcode at the collection date.
    + `tested_overall`: the nunber of covid tests administered by the collection date.
    + `count_overall_diff` and `tested_overall_diff`: the change in the number of cases or tests between the collection date, and the date exactly 14 days prior.
    + `rate_positive`: the proportion of tests that return positive, _after a shrinkage adjustment_. All `rate_` columns have a shrinkage adjustment applied to them.
* __Deaths__: this dataset captures deaths by the collection date. Note that not all deaths here are covid related.
* __Zipcode demographics__: this dataset includes a variety of zipcode-level features that may prove interesting for your analysis. Some more interesting features include:
    + The `population_race_*`, `population_age_*`, and so on columns, which capture demographic breakdowns.
    + Columns in this dataset are generally estimates captured by the Census.
    + All `*_moe` columns capture the 'margin of error' on their associated estimates.
    + Columns such as `uninsured_*` and `prop_essential_*`, may prove interesting for your analysis.

You have been provided with both `.csv` and `.feather` versions of these datasets. Both of these contain the same information.

<br>

# A week in the office

Here are the major checkpoints that you should aim for throughout the day. Look at the timeline (not in this document) for guidance on how long each section should take, but in general, the bulk of your day should be consumed by the self-directed research component. Remember that you should also be developing your presentation throughout the day. This probably won't contain all of the information that you discover: part of your responsibility as an RP is be critical about what information is useful or relevant.


## Data exploration

Begin your mini-project by exploring a dataset that we have prepared for you. Your first goal here is to understand, at a high-level and a low-level, what is contained in your data universe. Your second goal is to record this knowledge in report that summarizes what you have learned. This might take up a couple of pages, but will be mostly plots. You can learn about exploratory data analysis and see some ideas for things to include at [IBM](https://www.ibm.com/cloud/learn/exploratory-data-analysis), [Wikipedia](https://en.wikipedia.org/wiki/Exploratory_data_analysis), and a myriad of other sources.

At a minimum, try to achieve each of the following:

- Load the provided data into your program of choice (R, Python, Stata, whatever).
- Explore each dataset by looking at the columns and rows contained.
    + Find any datasets that can be joined together, and check the quality of the join.
    + For each dataset, determine what a single row represents in the real world.
    + For each dataset, identify which columns seem the most important and interesting to you.
- Summarise some high-level properties of the given datasets.
    + Density plots including histograms, density estimates, or ECDFs are all useful here.
    + How closely does this dataset resemble reality? For example, what is the average age, or distribution of race?
- Run some quality checks, to see if your dataset is sound and look for any weaknesses.
    + A good guide here is to think of properties that a good dataset would satisfy, but a poor one may not.
    + For example, the distribution of ages in different neighborhoods should vary in somewhat predictable ways.
- Put your findings into a report. Feel free to use your choice of tool; Google Docs, Microsoft Powerpoint, latex, RMarkdown, and Jupyter Notebooks are all appropriate.
    + There are some great resources for designing a report on the [World Bank](https://blogs.worldbank.org/impactevaluations/new-visual-libraries-r-and-stata-users) site.
    + Try to find the right balance of detailed and concise information. 
    + Imagine presenting to a faculty member who may not need to know information like the organisation of your data, but is very interested in knowing the content, quantity, and quality of information.

By the end of your data exploration, you should know in great detail exactly what data you have at hand. Many research projects will begin by exploring a dataset. Although, this isn't always the case: many begin with something more like a hypothesis, but that's not possible with our timeframe!


## Literature review

No project would be complete without contextualizing your work in the broader set of existing research. Your goal here is to identify other pieces of research on covid-19, or that use the same dataset you have. You should comment on how your work expands on, differs to, or fills in gaps with, these other pieces. You can also learn about how other researchers have approached a similar problem. The best pieces will be relevant to the self-directed research goal that you are planning for the later half of your day. This is a mini-project, so there's no need to go overboard: a couple of sources and one or two pages will be sufficient for this section. However, it might be prudent to return to your data exploration after seeing these other sources.

Try to achieve each of the following:

- Find two or three additional pieces of professional analysis on covid-19. Some suggestions are included below.
    + We are fortunate to work at a university with world-class colleagues, so one perfect place to start is the [report produced by the Urban Labs team](https://urbanlabs.uchicago.edu/news/where-covid-19-testing-lags-community-need-in-northeast-illinois).
    + The [COVID Tracking Project](https://covidtracking.com/analysis-updates)
    + [Becker Friedman Institute working papers](https://bfi.uchicago.edu/working-papers/?_topics=covid-19)
    + BPEA Conferences: [Summer 2020](https://www.brookings.edu/events/webinar-special-edition-bpea-2020-covid-19-and-the-economy/), [Fall 2020](https://www.brookings.edu/events/bpea-fall-2020-covid-19-and-the-economy/)
    + Articles by trusted researchers, such as [this](https://www.nytimes.com/2020/03/24/business/coronavirus-medical-supplies-regulations.html), or [this](https://www.nytimes.com/2020/03/19/business/small-businesses-coronavirus-help.html). (These may be behind a paywall.)
- Read the research examples you have chosen.
    + Identify if they agree with each other and your own expectations.
    + Try to recreate some figures and statistics with your own dataset. Does your dataset produce the same conclusion? Is there a good explanation for this difference? Can you test this explanation? Is it possible to rule out any other possible explanations using the data you have?
- Summarise your results, and add them to the report that you're generating.
- If you have time, try to augment your data universe with additional related data. 
    + The IDPH [has an API](https://idph.illinois.gov/DPHPublicInformation/Help) that can be used to extend the timeframe of your data, and the datasets you choose to collect. You'll actually find some pre-prepared R and Python snippets for pulling data from an API in the `experiments/temp-your_project_name/scratch/` folder of your experiment.
    + It's a good idea to recreate your previous analysis with this new dataset, to see how well the trends hold up in this new context.

By the end of this section, you should have a better understanding of where your impending research fits into the broader wealth of knowledge. Your dataset is probably different to the datasets used by the other authors you found in several ways. This might produce an interesting counterpoint as you continue your research.


## Self-guided project

Approaching topics you think are interesting and useful is one of the most rewarding and challenging parts of research. Your goal here will be to use your current knowledge to design an original contribution to the research community. Remember that you only have a single hackathon to complete this, so we you aren't expected to have a research paper at the end of the day. You should try to have a plan for your project, some tangible results, and a discussion about how your results matched your expectations.

We have provided some ideas for projects in the next section. Here, we list some goals you should strive to complete.

- Before starting, plan what you would like to achieve.
    + Include the goal of your mini-project, the impact of your mini-project, and the steps required to deliver your mini-project.
    + Add these to your report.
- Plan your remaining time to deliver your mini-project, and document the project in your report.
- Before you run out of time, add the outcome of your final project to your report. Remember that everybody will be sharing their work at the end of the hackathon.
- If you know how, we would love to see people add their research to a fork of the original git repository, and lodge a pull request to have their research included in the master branch.

By the end of this section, you should have produced some basic analysis towards achieiving your goal. Feel free to discuss how you would extend this analysis given more time: while you might not have time for a complete causal analysis today, maybe that's something you'd plan on doing in the future.


## Presenting your results

We value impactful research. An important part of this is being able to share your research with others, and to communicate your results constructively. An equally important aspect is being able to appreciate the work that others have done. To conclude the day, every team will present the results of their mini-project. We know that you only had a single day, so we understand if some things didn't get finished to the quality you'd like!

Here are some tips for your presentation:

- Presenting a report to a faculty member is very different to doing an assignment. A faculty member is likely more interested in your high-level approach and conclusions you've drawn, than the minutae contained in your data. Knowing what's important is a vital skill that you should exercise here.
- Try to contextualize your research in a broader stream of work, by using your literature review from the start of the day.
- It can be helpful to educate your audience about how you see the data. Even when other teams are as familiar as you are, you might be surprised how many people miss an interesting check that seemed obvious to you.
- When talking about your project, be sure to motivate it by talking about your goals, and why you think it is important.
- And a reminder: lots of research ends results you find disappointing. This doesn't make you a bad researcher. What you do with a dull or null result might, though.


<br>

# Ideas for self directed research

There's no limit to what you could produce for your mini-project, other than those imposed by time. We would be excited to see anything that captures your attention and motivates you to explore further. Here are some of the things that captured our attention:

* An evaluation of __policy rollouts__: consider the numerous changes in policy, such as the CARES Act, during the pandemic. Do we see any impact (immediate or long-term) on key outcomes? Give some intuition as to why or why not. Can we be sure that the relationships we see are causal? What might you do given more time/more data to ensure that these are causal relationships and not merely correlations.
* A __geographic data visualisation series__: a series of maps (or better yet, an interactive map) that illustrates the geographical distribution of important factors. What can (and can’t) we learn from this? What could public officials, policymakers, and economists do with this information? What are some different explanations for the patterns you see (eg. sorting, discrimination, climate patterns)?
* A report on __case drivers__: using the dataset on hand (and other sources if you like), can you describe the main drivers of better/worse outcomes for individuals? How did you avoid p-hacking and spurious correlations during this analysis? Can you frame an experiment that you would design to test your conclusions?
* A __dashboard__: create an experience targeted at a general audience, to help improve people’s understanding of covid-19. What stories are you trying to tell? Will it be interactive? Do you have a specific audience in mind? Is it interactive? There New York Times has some interesting examples, including [this article showing an easy way to understand risk](https://www.nytimes.com/interactive/2020/05/06/opinion/coronavirus-us-reopen.html), [this dashboard showing testing levels](https://www.nytimes.com/interactive/2020/us/coronavirus-testing.html), or [this dashboard looking at case counts across the country](https://www.nytimes.com/interactive/2020/us/coronavirus-us-cases.html).
* A __compartmental epidemiology model__: [compartmental models](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology), such as the SIR model, provide one of the most basic ways to model disease spread in a community. While _this type of model isn't sufficient to make serious predictions_, it would be interesting to see exactly where it fails, and what you would propose to remedy the model's inaccuracies.
* An __API data acquisition tool__: the IDPH has opened a new API endpoint for data access. Can you write a tool for your team that pulls data for them? You can [access the API details here](https://idph.illinois.gov/DPHPublicInformation/Help). In addition, you'll find some R and Python snippets for pulling data from an API in the `scratch` folder of your experiment.
* A __case count smoother__: cases vary significantly by day. Can you build a tool that tracks the overall rate of cases? What approach are you taking? Some advanced questions: it is well known that weekends have fewer counts than other days, does your tool address this? There are also spikes at various points in the time series, does your approach address this? Somebody else has a tool that uses a different smoothing approach, how can you decide whose approach is better?


<br>

# Repository structure

This repository is organized so that teams wishing to merge their work back into the main branch can do so with minimal conflict. We ask that you try to keep the pattern set out below. Try to keep your project files in `experiments/[your_project_name]` (after renaming the directory), and your data in a directory like `data/clean/[your_project_name]`. This means that we can accommodate multiple projects merging with the main repository after the hackathon, if we have people willing to lodge pull requests.

<!-- recreate with `tree \-\-filelimit 15` -->
```
.
├── README.md
├── config.mk
├── Makefile
├── data
│   ├── clean
│   │   └── <clean data sets ready for direct import and analysis>
│   └── raw
│       └── <immutable data, collected from source-of-truth>
├── experiments
│   └── [your_project_name]
│       ├── README.md  # include a README with your project
│       ├── utils
│       │   └── <code imported by your scripts with general functionality>
│       ├── scripts
│       │   └── <code for the current experiment to produce any analysis, data, or other outputs>
│       ├── outputs
│       │   └── <analysis output, ready for sharing and presentation>
│       └── scratch
│           └── <adhoc files, ignored>
├── [.venv|renv|renv.lock|.envrc|...]  # directories and files created by various virtual environment managers
└── Booth-Hackathon-2021.Rproj  # created by RStudio
```

# directories used for different make commands
TEST_DIR = tests
EXPERIMENT_DIR = experiments

# specific paths for executors. Helps when managing virtual environemnts.
# uncomment below lines which paths to virtual environemnt tools if needed.
# PYTHON =
# R =

# testing vars
LINTING_LINELENGTH = 120

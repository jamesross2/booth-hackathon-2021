"""Some snippets of Python code that might help be helpful for a Python project."""

import json
import pathlib

import pandas
import requests

# baseline URL used in all queries
# follow this URL to find help pages and a list of available API endpoints!
url_base = "https://idph.illinois.gov/DPHPublicInformation/"

# pipeline for extracting information from an API
url_end = "api/COVIDVaccine/getVaccineAdministrationCurrent"
api_results_raw = requests.get(url_base + url_end)
api_results_json = api_results_raw.json()

# to inspect results, check keys to see what information we have, and use pandas to convert to tabular form
api_results_json.keys()
pandas.json_normalize(api_results_json["lastUpdatedDate"])
pandas.json_normalize(api_results_json["VaccineAdministration"])

# extract information from an API with query parameters
url_end = "api/COVIDVaccine/getVaccineAdministration"
params = {"CountyName": "Cook"}
api_results_raw = requests.get(url_base + url_end, params=params)
api_results_json = api_results_raw.json()

# to inspect results, check keys to see what information we have, and use pandas to convert to tabular form
api_results_json.keys()
pandas.json_normalize(api_results_json["lastUpdatedDate"])
pandas.json_normalize(api_results_json["VaccineAdministration"])
pandas.json_normalize(api_results_json["CurrentVaccineAdministration"])

# save the resulting dataset
pathlib.Path("data", "raw").mkdir(mode=777, parents=True, exist_ok=True)
pandas.json_normalize(api_results_json["VaccineAdministration"]).to_feather(pathlib.Path("data", "raw", "vaccine_admin_cook.feather"))
# alternative for csv filetypes: use `to_csv` instead of feather

#!/usr/bin/make -f
SHELL = /bin/sh
include config.mk
.DELETE_ON_ERROR:

# boilerplate variables, do not edit
MAKEFILE_PATH := $(abspath $(firstword $(MAKEFILE_LIST)))
MAKEFILE_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

# required values, set to defaults here if not given in config.mk
TEST_DIR ?= tests
EXPERIMENT_DIR ?= experiments
LINTING_LINELENGTH ?= 120
R ?= $(shell which R)
PYTHON ?= $(shell which python3)

# firm variables, usually the same but potentially require editing for current repo
R_FILE_INCLUDED_PATTERNS := c('R', 'Rmd', 'Rprofile')
R_FILE_INCLUDED_REGEX := '\\\\.(R|Rmd|Rprofile)'
LINT_DIR_EXCLUDED_PATTERNS := c('packrat', '.venv', 'renv')
LINT_DIR_EXCLUDED_REGEX := '(packrat|.venv|renv)'
CLEAN_DIR_LIST_INCLUDED = $(TEST_DIR) $(EXPERIMENT_DIR)
CLEAN_DIR_REGEX_INCLUDED = $(TEST_DIR)|$(EXPERIMENT_DIR)

# local variables for this script (edit away)

# use regex to go through this Makefile and print help lines
# help lines is any comment starting with double '#' (see next comment). Prints alphabetical order.
## help :		print this help.
.PHONY: help
help: Makefile
	@echo "\nBooth School of Business Hackathon 2021."
	@echo "\n	Generic commands"
	@sed -n 's/^## /		/p' $< | sort

## variables : 	list all variables in the Makefile.
.PHONY: variables
variables:
	@echo MAKEFILE_PATH: $(MAKEFILE_PATH)
	@echo MAKEFILE_DIR: $(MAKEFILE_DIR)
	@echo R: $(R)
	@echo PYTHON: $(PYTHON)

## clean : 	remove temporary files from repo.
delete-pattern = find . -regex 'pattern' -delete
.PHONY: clean scrub
clean:
	@$(subst pattern,^./($(CLEAN_DIR_REGEX_INCLUDED))/.*\.py[co],$(delete-pattern))
	@$(subst pattern,^./($(CLEAN_DIR_REGEX_INCLUDED))/.*__pycache__,$(delete-pattern))
	@$(subst pattern,^/($(CLEAN_DIR_REGEX_INCLUDED))/.*_files/figure-html,$(delete-pattern))

## format : 	apply style scripts to Python and R scripts; also try format-r, format-py, format-isort.
.PHONY: format-py format-r format
format-py: clean
	$(PYTHON) -m black --include="($(CLEAN_DIR_REGEX_INCLUDED)).*\.pyi?$$" --line-length $(LINTING_LINELENGTH) .
	$(PYTHON) -m isort --line-width $(LINTING_LINELENGTH) $(CLEAN_DIR_LIST_INCLUDED)
format-r: clean
	# note: have to use dev version of styler in order to keep exclude_dirs working recursively, as intended
	$(R) --silent -e "styler::style_dir(path = '$(MAKEFILE_DIR)', exclude_dirs = $(LINT_DIR_EXCLUDED_PATTERNS), filetype = $(R_FILE_INCLUDED_PATTERNS))"
format: format-py format-r

## lint :		test that code adheres to style guides.
.PHONY: lint-py lint-r lint
lint-py: clean
	$(PYTHON) -m black --check --include="($(CLEAN_DIR_REGEX_INCLUDED))/.*\pyi?$$" --line-length $(LINTING_LINELENGTH) .
lint-r: clean
	$(R) --silent -e "lintr::lint_dir(path = '$(MAKEFILE_DIR)', exclusions = $(LINT_DIR_EXCLUDED_REGEX), pattern = $(R_FILE_INCLUDED_REGEX))"
lint: lint-py lint-r
